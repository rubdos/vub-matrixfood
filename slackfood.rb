#!/usr/bin/env ruby

require 'bundler/setup'

require "rubygems"
require "net/http"
require "pp"
require "time"
require "cgi"

require "json"
require "matrix_sdk"

VUB_RESTO_URL_EN = URI("https://vub-resto.opencloudedge.be/v1/etterbeek.en.json")
VUB_RESTO_URL_NL = URI("https://vub-resto.opencloudedge.be/v1/etterbeek.nl.json")

TUSSENFIX = "<br />\n"

EMOJI = {
  "soup" => "🥣",
  "soep" => "🥣",
  "menu 1" => "🍲",
  "menu 2" => "🍳",
  "veggie" => "🥗",
  "fish" => "🐟",
  "vis" => "🐟",
  "pasta" => "🍝",
  "wok" => "🥢",
}
EMOJI.default = "🥙"

def get_JSON(url)
  res = nil
  uri = URI(url)

  Net::HTTP.start(uri.host, uri.port,
                  :use_ssl => uri.scheme == "https") do |http|
    request = Net::HTTP::Get.new uri.request_uri

    res = http.request request
    return res.body
  end
end

def postit(data_en, data_nl, location)
  client = MatrixSdk::Client.new ENV["HOST"]
  client.login ENV["USERNAME"], ENV["PASSWORD"]

  channel = client.find_room ENV["CHANNEL"]

  vandaag = Date.today
  dagstr = vandaag.strftime("%F")
  parsed_data_nl = JSON.parse(data_nl)
  parsed_data_en = JSON.parse(data_en)

  menus = {}

  parsed_data_en.each do |node|
    next unless node["date"] == dagstr

    items = node["menus"]
    until items.empty?
      dish = items.shift
      if not menus[dish["name"].downcase]
        menus[dish["name"].downcase] = {}
      end
      menus[dish["name"].downcase]["en"] = dish["dish"]
    end
    break
  end

  parsed_data_nl.each do |node|
    next unless node["date"] == dagstr

    items = node["menus"]
    until items.empty?
      dish = items.shift
      if not menus[dish["name"].downcase]
        menus[dish["name"].downcase] = {}
      end
      menus[dish["name"].downcase]["nl"] = dish["dish"]
    end
    break
  end

  if menus.empty?
    return
  end

  lines_en = ["<strong>🇬🇧 English</strong>"]
  lines_nl = ["<strong>🇳🇱 Nederlands</strong>"]

  menus.each do |name, dish|
    nl = dish["nl"]
    if ENV["EN_JOKE"].nil?
      en = dish["en"]
    else
      en = ENV["EN_JOKE"]
    end
    lines_en.push("<em>#{EMOJI[name]} #{name.capitalize}</em>: #{en}")
    lines_nl.push("<em>#{EMOJI[name]} #{name.capitalize}</em>: #{nl}")
  end
  lines = lines_en.join(TUSSENFIX) + TUSSENFIX + TUSSENFIX + lines_nl.join(TUSSENFIX)
  channel.send_html lines
end

if ENV["VUBFOOD_LOCATION"].nil?
  warn "location not set, assume etterbeek"
  location = :etterbeek
elsif ENV["VUBFOOD_LOCATION"].downcase == "etterbeek"
  location = :etterbeek
elsif ENV["VUBFOOD_LOCATION"].downcase == "jette"
  location = :jette
end

postit(get_JSON(VUB_RESTO_URL_EN), get_JSON(VUB_RESTO_URL_NL), location)
